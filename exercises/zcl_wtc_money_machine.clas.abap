CLASS zcl_wtc_money_machine DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    TYPES: BEGIN OF ENUM unit,
             five_hundred_euro_note VALUE 500,
             two_hundred_euro_note  VALUE 200,
             one_hundred_euro_note  VALUE 100,
             fifty_euro_note        VALUE 50,
             twenty_euro_note       VALUE 20,
             ten_euro_note          VALUE 10,
             five_euro_note         VALUE 5,
             two_euro_coin          VALUE 2,
             one_euro_coin          VALUE 1,
             zero                   VALUE IS INITIAL,
           END OF ENUM unit,
           tt_units TYPE STANDARD TABLE OF unit
                         WITH NON-UNIQUE DEFAULT KEY.

    TYPES: BEGIN OF ty_change,
             amount TYPE i,
             type   TYPE unit,
           END OF ty_change,
           tt_change TYPE STANDARD TABLE OF ty_change
                          WITH NON-UNIQUE DEFAULT KEY.

    METHODS:
      constructor,
      get_change
        IMPORTING
          i_amount        TYPE i
        RETURNING
          VALUE(r_change) TYPE tt_change.

  PRIVATE SECTION.
    DATA: possible_units TYPE zcl_wtc_money_machine=>tt_units.

ENDCLASS.



CLASS zcl_wtc_money_machine IMPLEMENTATION.


  METHOD constructor.

    possible_units = VALUE tt_units(
                       ( five_hundred_euro_note )
                       ( two_hundred_euro_note )
                       ( one_hundred_euro_note )
                       ( fifty_euro_note )
                       ( twenty_euro_note )
                       ( ten_euro_note )
                       ( five_euro_note )
                       ( two_euro_coin )
                       ( one_euro_coin ) ).

  ENDMETHOD.


  METHOD get_change.

    DATA(rest_amount) = i_amount.

    LOOP AT possible_units ASSIGNING FIELD-SYMBOL(<possible_unit>).

      DATA(current_unit) = CONV i( <possible_unit> ).

      DATA(ls_change) = VALUE ty_change(
                          type   = <possible_unit>
                          amount = REDUCE i( INIT result = 0
                                             FOR l_rest_amount = rest_amount
                                             THEN l_rest_amount - current_unit
                                             WHILE l_rest_amount >= current_unit
                                             NEXT result = result + 1 ) ).

      IF ls_change-amount IS NOT INITIAL.
        rest_amount = rest_amount - ls_change-amount * current_unit.
        INSERT ls_change INTO TABLE r_change.
      ENDIF.

    ENDLOOP.

  ENDMETHOD.
ENDCLASS.
