*"* use this source file for your ABAP unit test classes

CLASS ltcl_money_machine DEFINITION DEFERRED.

CLASS zcl_wtc_money_machine DEFINITION LOCAL FRIENDS ltcl_money_machine.

CLASS ltcl_money_machine DEFINITION FINAL FOR TESTING
  DURATION SHORT
  RISK LEVEL HARMLESS.

  PRIVATE SECTION.
    DATA:
      m_amount_input  TYPE i,
      m_cut           TYPE REF TO zcl_wtc_money_machine,
      m_change_output TYPE zcl_wtc_money_machine=>tt_change.

    METHODS:
      setup,
      minus_eleven FOR TESTING RAISING cx_static_check,
      zero FOR TESTING RAISING cx_static_check,
      one_euro FOR TESTING RAISING cx_static_check,
      two_euro FOR TESTING RAISING cx_static_check,
      three_euro FOR TESTING RAISING cx_static_check,
      five_euro FOR TESTING RAISING cx_static_check,
      nine_euro FOR TESTING RAISING cx_static_check,
      ten_euro FOR TESTING RAISING cx_static_check,
      eighteen_euro FOR TESTING RAISING cx_static_check,
      twenty_euro FOR TESTING RAISING cx_static_check,
      forty_nine_euro FOR TESTING RAISING cx_static_check,
      fifty_nine_euro FOR TESTING RAISING cx_static_check,
      eigthy_four_euro FOR TESTING RAISING cx_static_check,
      one_hundred_euro FOR TESTING RAISING cx_static_check,
      one_hundred_thirty_three FOR TESTING RAISING cx_static_check,
      two_hundred_euro FOR TESTING RAISING cx_static_check,
      four_hundred_fourty_four FOR TESTING RAISING cx_static_check,
      five_hundred_euro FOR TESTING RAISING cx_static_check,
      six_hundred_eigthy_five FOR TESTING RAISING cx_static_check,

      _given_amount_is
        IMPORTING
          i_amount_input TYPE i,

      _when_get_change_is_calculated,

      _then_amount_should_be
        IMPORTING
          i_expected_amount TYPE zcl_wtc_money_machine=>tt_change.

ENDCLASS.


CLASS ltcl_money_machine IMPLEMENTATION.

  METHOD setup.

    m_cut = NEW #( ).

  ENDMETHOD.

  METHOD minus_eleven.

    _given_amount_is( -11 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ) ).

  ENDMETHOD.

  METHOD zero.

    _given_amount_is( 0 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ) ).

  ENDMETHOD.

  METHOD one_euro.

    _given_amount_is( 1 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>one_euro_coin ) ) ).

  ENDMETHOD.

  METHOD two_euro.

    _given_amount_is( 2 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>two_euro_coin ) ) ).

  ENDMETHOD.

  METHOD three_euro.

    _given_amount_is( 3 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>two_euro_coin )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>one_euro_coin ) ) ).

  ENDMETHOD.


  METHOD five_euro.

    _given_amount_is( 5 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>five_euro_note ) ) ).

  ENDMETHOD.

  METHOD nine_euro.

    _given_amount_is( 9 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>five_euro_note )
                                     ( amount = 2
                                       type   = zcl_wtc_money_machine=>two_euro_coin ) ) ).

  ENDMETHOD.


  METHOD ten_euro.

    _given_amount_is( 10 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>ten_euro_note ) ) ).

  ENDMETHOD.


  METHOD eighteen_euro.

    _given_amount_is( 18 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>ten_euro_note )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>five_euro_note )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>two_euro_coin )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>one_euro_coin ) ) ).

  ENDMETHOD.

  METHOD twenty_euro.

    _given_amount_is( 20 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>twenty_euro_note ) ) ).

  ENDMETHOD.


  METHOD forty_nine_euro.

    _given_amount_is( 49 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 2
                                       type   = zcl_wtc_money_machine=>twenty_euro_note )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>five_euro_note )
                                     ( amount = 2
                                       type   = zcl_wtc_money_machine=>two_euro_coin ) ) ).

  ENDMETHOD.

  METHOD fifty_nine_euro.

    _given_amount_is( 50 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>fifty_euro_note ) ) ).

  ENDMETHOD.

  METHOD eigthy_four_euro.

    _given_amount_is( 84 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>fifty_euro_note )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>twenty_euro_note )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>ten_euro_note )
                                     ( amount = 2
                                       type   = zcl_wtc_money_machine=>two_euro_coin ) ) ).

  ENDMETHOD.


  METHOD one_hundred_euro.

    _given_amount_is( 100 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>one_hundred_euro_note ) ) ).

  ENDMETHOD.

  METHOD one_hundred_thirty_three.

    _given_amount_is( 133 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>one_hundred_euro_note )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>twenty_euro_note )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>ten_euro_note )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>two_euro_coin )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>one_euro_coin ) ) ).

  ENDMETHOD.

  METHOD two_hundred_euro.

    _given_amount_is( 200 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>two_hundred_euro_note ) ) ).

  ENDMETHOD.

  METHOD four_hundred_fourty_four.

    _given_amount_is( 444 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 2
                                       type   = zcl_wtc_money_machine=>two_hundred_euro_note )
                                     ( amount = 2
                                       type   = zcl_wtc_money_machine=>twenty_euro_note )
                                     ( amount = 2
                                       type   = zcl_wtc_money_machine=>two_euro_coin ) ) ).

  ENDMETHOD.

  METHOD five_hundred_euro.

    _given_amount_is( 500 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>five_hundred_euro_note ) ) ).

  ENDMETHOD.

  METHOD six_hundred_eigthy_five.

    _given_amount_is( 685 ).
    _when_get_change_is_calculated( ).
    _then_amount_should_be( VALUE #( ( amount = 1
                                       type   = zcl_wtc_money_machine=>five_hundred_euro_note )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>one_hundred_euro_note )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>fifty_euro_note )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>twenty_euro_note )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>ten_euro_note )
                                     ( amount = 1
                                       type   = zcl_wtc_money_machine=>five_euro_note ) ) ).

  ENDMETHOD.

  METHOD _given_amount_is.

    m_amount_input = i_amount_input.

  ENDMETHOD.


  METHOD _when_get_change_is_calculated.

    m_change_output = m_cut->get_change( m_amount_input ).

  ENDMETHOD.


  METHOD _then_amount_should_be.

    cl_abap_unit_assert=>assert_equals(
      act = m_change_output
      exp = i_expected_amount
      msg = |For given amount { m_amount_input }|
         && | an unexpected result was calculated| ).

  ENDMETHOD.

ENDCLASS.
