*"* use this source file for your ABAP unit test classes

CLASS test_roman_converter DEFINITION FINAL FOR TESTING
  DURATION SHORT
  RISK LEVEL HARMLESS.

  PRIVATE SECTION.
    TYPES: BEGIN OF ty_test_case,
             roman  TYPE string,
             arabic TYPE i,
           END OF ty_test_case,
           tty_test_case TYPE STANDARD TABLE OF ty_test_case
                              WITH NON-UNIQUE DEFAULT KEY.
    DATA:
      m_cut         TYPE REF TO zcl_wtc_roman_converter,
      mt_test_cases TYPE tty_test_case.

    METHODS:
      setup,
      test_roman_to_arabic FOR TESTING RAISING cx_static_check.

ENDCLASS.


CLASS test_roman_converter IMPLEMENTATION.

  METHOD setup.

    "given
    m_cut = NEW zcl_wtc_roman_converter( ).

  ENDMETHOD.

  DEFINE test_cases_are.

    INSERT VALUE #( roman  = &1
                    arabic = &2 )
           INTO TABLE mt_test_cases.

  END-OF-DEFINITION.

  METHOD test_roman_to_arabic.

    " Given
    test_cases_are:
      "roman    arabic
        'AA'     -1,
        'IIII'   -1,
        'VX'     -1,
        ''       -1,
        ' '      -1,
        'I'       1,
        'II'      2,
        'III'     3,
        'IV'      4,
        'V'       5,
        'VI'      6,
        'VII'     7,
        'VIII'    8,
        'IX'      9,
        'X'       10,
        'L'       50,
        'C'       100,
        'CC'      200,
        'D'       500,
        'M'       1000,
        'MMVII'   2007.

    LOOP AT mt_test_cases ASSIGNING FIELD-SYMBOL(<test_case>).

      "when
      DATA(arabic) = m_cut->to_arabic( <test_case>-roman ).

      "then
      cl_abap_unit_assert=>assert_equals( act = arabic
                                          exp = <test_case>-arabic
                                          msg = |Roman { <test_case>-roman } should be arabic { <test_case>-arabic }| ).

    ENDLOOP.

  ENDMETHOD.

ENDCLASS.
